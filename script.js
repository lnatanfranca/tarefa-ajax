// URL da API

const url = "http://treinamento-ajax-api.herokuapp.com";

// Elementos

const botaoEnviar = document.querySelector(".botao-enviar")
const msgHist = document.querySelector(".mensagens");
const txtMsg = document.querySelector(".texto");

// função para mostar o histórico de mensagens

const mostraMsgs = () => {
    fetch(url + "/messages")
    .then((response) => response.json())
    .then((messages) => {
        messages.forEach(message => createPost(message))
    })
    .catch(error => console.error(error))
    .finally(() => console.log('requisição finalizada'));
}

// função para inserir a mensagem no histórico

const createPost = (msg) => {
    const li = document.createElement("li")

    li.className = "mensagem"
    li.innerHTML = `<h3>${msg.author}</h3>
            <p class="texto-msg">${msg.content}</p>
            <div class="botoes">
                <input type="button" value="Editar" class="Editar botao-msg" onclick="editar(this)")>
                <input type="button" value="Deletar" class="Deletar botao-msg" onclick="deletar(this)")>
                <input type="text" class="id" value="${msg.id}" hidden>
            </div>
            <div class="botoes-modo-editar" hidden>
                <input>
                <input type="button" value="Enviar" class="Enviar botao-msg" onclick="editarTexto(this)">
            </div>`

    msgHist.appendChild(li)
}

// função para enviar uma mensagem

botaoEnviar.addEventListener("click", () => {
    const mensagem = txtMsg.value;

    const body = {
        message: {
            content: mensagem,
            author: "Lucas Costa"
        }
    }

    const config = {
        method: "POST", 
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(body)
    }

    fetch((url + "/messages"), config)
    .then((response) => response.json())
    .then((message) => {
        alert("msg criada");
        createPost(message);
        console.log(message);
    })
    .catch(err => console.error(err));

    txtMsg.value = "";
})

// função para deletar a mensagem

function deletar(elemento) {
    const parent = elemento.parentNode.parentNode;
    const idMsg = parent.children[2].children[2].value;

    const config = {
        method: "DELETE"
    }

    console.log(idMsg);

    fetch(url + "/messages/" + idMsg, config)
    .catch(err => console.error(err));

    parent.remove();
}

function editar(elemento) {
    const parent = elemento.parentNode.parentNode

    // Esconder os botões normais
    parent.querySelector(".botoes").toggleAttribute("hidden")

    // Mostrar os botões de edição
    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")
}

// função para editar mensagem

function editarTexto(elemento) {
    const parent = elemento.parentNode.parentNode;
    const textArea = elemento.parentNode.children[0];
    const msgEditada = textArea.value;
    const idMsg = parent.children[2].children[2].value;

    const body = {
        message: {
            content: msgEditada,
            author: "Lucas Costa"
        }
    }

    const config = {
        method: "PATCH", 
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(body)
    }
    
    fetch(url + "/messages/" + idMsg, config)
    .then((response) => response.json())
    .then((message) => {
        alert('msg editada');
        console.log(message);
    }).catch(err => console.error(err));

    // Mostrar na tela a edição
    elemento.parentNode.parentNode.children[1].innerText = msgEditada;

    // Esconder os elementos de editar novamente
    parent.querySelector(".botoes").toggleAttribute("hidden")

    // Tirar o atributo hidden dos botões Editar e Deletar
    parent.querySelector(".botoes-modo-editar").toggleAttribute("hidden")

    // Reseta o texto
    textArea.value = ""
}

// Inicializando

mostraMsgs();